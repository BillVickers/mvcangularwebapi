﻿using MVCAngularWebAPI.Models;
using System.Collections.Generic;

namespace MVCAngularWebAPI.Interface
{
    interface IProductRepository
    {
        IEnumerable<TblProductList> GetAll();
        TblProductList Get(int id);
        TblProductList Add(TblProductList item);
        bool Update(TblProductList item);
        bool Delete(int id);
    }
}
